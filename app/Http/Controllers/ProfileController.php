<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();
        $data_profile = User::find($user_id);

        return view('auth.profile', ['data_profile' => $data_profile]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'gender' => 'required|in:pria,wanita',
            'username' => [
                'required',
                'unique:users,username,' . $request->user()->id,
            ],
        ]);

        $user_id = Auth::id();

        $data = User::find($user_id);

        $data->name = $request->name;
        $data->gender = $request->gender;

        // Update data di model User (jika email berubah)
        if ($data->username !== $request->username) {
            $data->username = $request->username;
        }
        $data->save();

        return redirect('/profile')->with('success', 'Profile Berhasil di Perbarui');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'passwordold' => 'required',
            'passwordnew' => 'required|min:8',
            'passwordconfirm' => 'required',
        ]);

        // Simpan password baru ke dalam database
        $user_id = Auth::id();

        $user = User::find($user_id);

        // Periksa apakah password lama cocok dengan yang ada di database
        if (Hash::check($request->passwordold, $user->password)) {
            // Jika cocok, simpan password baru yang dihash
            $user->password = Hash::make($request->passwordnew);
            $user->save();
            return redirect('/profile')->with('success', 'Password Berhasil di Update');
        } else {
            // Jika tidak cocok, kembalikan dengan pesan error
            return redirect('/profile')->with('error', 'Maaf Password yang Anda Masukkan Salah');
        }
    }
}
