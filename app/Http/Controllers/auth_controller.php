<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class auth_controller extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'gender' => 'required|string|in:pria,wanita',
            'username' => 'required|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = new User([
            'name' => $request->input('name'),
            'gender' => $request->input('gender'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
        ]);
        $user->save();

        return redirect('/')->with('success', 'Registrasi berhasil!');
    }

    public function login(Request $request)
    {
        // Validasi input
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        // Coba melakukan otentikasi
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            // Otentikasi berhasil
            return redirect('/dashboard')->with('success', 'Login berhasil');
        }

        // Otentikasi gagal
        return redirect()->route('login')->with('error', 'Email atau password salah');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
