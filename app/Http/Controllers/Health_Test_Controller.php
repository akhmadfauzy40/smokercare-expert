<?php

namespace App\Http\Controllers;

use App\Models\Hasil_diagnosa;
use App\Models\Profile;
use App\Models\Test_gejala;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Health_Test_Controller extends Controller
{

    public function index()
    {
        $data = Test_gejala::where('user_id', Auth::id())->latest()->get();

        return view('hasil-tes.index', ['data' => $data]);
    }

    public function detail($id)
    {
        $data = Hasil_diagnosa::find($id);
        $edukasi = json_decode($data->edukasi);

        $test = Test_gejala::find($data->test_gejala_id);

        $cf_g01 = 0.5;
        $cf_g02 = 0.8;
        $cf_g03 = 0.6;
        $cf_g04 = 0.6;
        $cf_g05 = 0.6;
        $cf_g06 = 0.8;
        $cf_g07 = 0.8;
        $cf_g08 = 0.8;
        $cf_g09 = 0.6;
        $cf_g10 = 0.6;
        $cf_g11 = 1;
        $cf_g12 = 0.5;
        $cf_g13 = 0.6;
        $cf_g14 = 0.6;
        $cf_g15 = 0.8;
        $cf_g16 = 1;
        $cf_g17 = 0.8;
        $cf_g18 = 1;
        $cf_g19 = 0.8;
        $cf_g20 = 1;
        $cf_g21 = 0.8;

        $hasil_g01 = $test->g01 * $cf_g01;
        $hasil_g02 = $test->g02 * $cf_g02;
        $hasil_g03 = $test->g03 * $cf_g03;
        $hasil_g04 = $test->g04 * $cf_g04;
        $hasil_g05 = $test->g05 * $cf_g05;
        $hasil_g06 = $test->g06 * $cf_g06;
        $hasil_g07 = $test->g07 * $cf_g07;
        $hasil_g08 = $test->g08 * $cf_g08;
        $hasil_g09 = $test->g09 * $cf_g09;
        $hasil_g10 = $test->g10 * $cf_g10;
        $hasil_g11 = $test->g11 * $cf_g11;
        $hasil_g12 = $test->g12 * $cf_g12;
        $hasil_g13 = $test->g13 * $cf_g13;
        $hasil_g14 = $test->g14 * $cf_g14;
        $hasil_g15 = $test->g15 * $cf_g15;
        $hasil_g16 = $test->g16 * $cf_g16;
        $hasil_g17 = $test->g17 * $cf_g17;
        $hasil_g18 = $test->g18 * $cf_g18;
        $hasil_g19 = $test->g19 * $cf_g19;
        $hasil_g20 = $test->g20 * $cf_g20;
        $hasil_g21 = $test->g21 * $cf_g21;

        // perhitungan untuk pneumonia
        $proc1_pneumonia = $hasil_g01 + $hasil_g02 * (1 - $hasil_g01);
        $proc2_pneumonia = $proc1_pneumonia + $hasil_g03 * (1 - $proc1_pneumonia);
        $proc3_pneumonia = $proc2_pneumonia + $hasil_g04 * (1 - $proc2_pneumonia);
        $hasil_pneumonia = $proc3_pneumonia + $hasil_g11 * (1 - $proc3_pneumonia);

        // perhitungan untuk bronkitis
        $proc1_bronkitis = $hasil_g01 + $hasil_g05 * (1 - $hasil_g01);
        $proc2_bronkitis = $proc1_bronkitis + $hasil_g04 * (1 - $proc1_bronkitis);
        $proc3_bronkitis = $proc2_bronkitis + $hasil_g06 * (1 - $proc2_bronkitis);
        $proc4_bronkitis = $proc3_bronkitis + $hasil_g07 * (1 - $proc3_bronkitis);
        $proc5_bronkitis = $proc4_bronkitis + $hasil_g08 * (1 - $proc4_bronkitis);
        $proc6_bronkitis = $proc5_bronkitis + $hasil_g09 * (1 - $proc5_bronkitis);
        $hasil_bronkitis = $proc6_bronkitis + $hasil_g10 * (1 - $proc6_bronkitis);

        // perhitungan untuk tuberkolosis paru
        $proc1_tuberkolosis_paru = $hasil_g03 + $hasil_g04 * (1 - $hasil_g03);
        $proc2_tuberkolosis_paru = $proc1_tuberkolosis_paru + $hasil_g05 * (1 - $proc1_tuberkolosis_paru);
        $proc3_tuberkolosis_paru = $proc2_tuberkolosis_paru + $hasil_g06 * (1 - $proc2_tuberkolosis_paru);
        $proc4_tuberkolosis_paru = $proc3_tuberkolosis_paru + $hasil_g12 * (1 - $proc3_tuberkolosis_paru);
        $proc5_tuberkolosis_paru = $proc4_tuberkolosis_paru + $hasil_g13 * (1 - $proc4_tuberkolosis_paru);
        $proc6_tuberkolosis_paru = $proc5_tuberkolosis_paru + $hasil_g14 * (1 - $proc5_tuberkolosis_paru);
        $proc7_tuberkolosis_paru = $proc6_tuberkolosis_paru + $hasil_g15 * (1 - $proc6_tuberkolosis_paru);
        $hasil_tuberkolosis_paru = $proc7_tuberkolosis_paru + $hasil_g16 * (1 - $proc7_tuberkolosis_paru);

        // perhitungan untuk emfisema
        $proc1_emfisema = $hasil_g01 + $hasil_g03 * (1 - $hasil_g01);
        $proc2_emfisema = $proc1_emfisema + $hasil_g04 * (1 - $proc1_emfisema);
        $proc3_emfisema = $proc2_emfisema + $hasil_g10 * (1 - $proc2_emfisema);
        $proc4_emfisema = $proc3_emfisema + $hasil_g17 * (1 - $proc3_emfisema);
        $hasil_emfisema = $proc4_emfisema + $hasil_g18 * (1 - $proc4_emfisema);

        // perhitungan untuk abses paru
        $proc1_abses_paru = $hasil_g02 + $hasil_g19 * (1 - $hasil_g02);
        $proc2_abses_paru = $proc1_abses_paru + $hasil_g20 * (1 - $proc1_abses_paru);
        $hasil_abses_paru = $proc2_abses_paru + $hasil_g21 * (1 - $proc2_abses_paru);

        $data_langkah = [
            'cf_user_cf_pakar' => [
                'Total_G01 = ' . $test->g01 . ' * ' . $cf_g01 . ' = ' . $hasil_g01,
                'Total_G01 = ' . $test->g02 . ' * ' . $cf_g02 . ' = ' . $hasil_g02,
                'Total_G01 = ' . $test->g03 . ' * ' . $cf_g03 . ' = ' . $hasil_g03,
                'Total_G01 = ' . $test->g04 . ' * ' . $cf_g04 . ' = ' . $hasil_g04,
                'Total_G01 = ' . $test->g05 . ' * ' . $cf_g05 . ' = ' . $hasil_g05,
                'Total_G01 = ' . $test->g06 . ' * ' . $cf_g06 . ' = ' . $hasil_g06,
                'Total_G01 = ' . $test->g07 . ' * ' . $cf_g07 . ' = ' . $hasil_g07,
                'Total_G01 = ' . $test->g08 . ' * ' . $cf_g08 . ' = ' . $hasil_g08,
                'Total_G01 = ' . $test->g09 . ' * ' . $cf_g09 . ' = ' . $hasil_g09,
                'Total_G01 = ' . $test->g10 . ' * ' . $cf_g10 . ' = ' . $hasil_g10,
                'Total_G01 = ' . $test->g11 . ' * ' . $cf_g11 . ' = ' . $hasil_g11,
                'Total_G01 = ' . $test->g12 . ' * ' . $cf_g12 . ' = ' . $hasil_g12,
                'Total_G01 = ' . $test->g13 . ' * ' . $cf_g13 . ' = ' . $hasil_g13,
                'Total_G01 = ' . $test->g14 . ' * ' . $cf_g14 . ' = ' . $hasil_g14,
                'Total_G01 = ' . $test->g15 . ' * ' . $cf_g15 . ' = ' . $hasil_g15,
                'Total_G01 = ' . $test->g16 . ' * ' . $cf_g16 . ' = ' . $hasil_g16,
                'Total_G01 = ' . $test->g17 . ' * ' . $cf_g17 . ' = ' . $hasil_g17,
                'Total_G01 = ' . $test->g18 . ' * ' . $cf_g18 . ' = ' . $hasil_g18,
                'Total_G01 = ' . $test->g19 . ' * ' . $cf_g19 . ' = ' . $hasil_g19,
                'Total_G01 = ' . $test->g20 . ' * ' . $cf_g20 . ' = ' . $hasil_g20,
                'Total_G01 = ' . $test->g21 . ' * ' . $cf_g21 . ' = ' . $hasil_g21,
            ],
            'langkah_pneumonia' => [
                'Proc 1 (G01 & G02)    = ' . $hasil_g01 . ' + ' . $hasil_g02 . ' * (1 - ' . $hasil_g01 . ')',
                'Proc 2 (Proc 1 & G03) = ' . $proc1_pneumonia . ' + ' . $hasil_g03 . ' * (1 - ' . $proc1_pneumonia . ')',
                'Proc 3 (Proc 2 & G04) = ' . $proc2_pneumonia . ' + ' . $hasil_g04 . ' * (1 - ' . $proc2_pneumonia . ')',
                'Proc 4 (Proc 3 & G11) = ' . $proc3_pneumonia . ' + ' . $hasil_g11 . ' * (1 - ' . $proc3_pneumonia . ')',
                'Hasil = ' . $hasil_pneumonia
            ],
            'langkah_bronkitis' => [
                'Proc 1 (G01 & G04)    = ' . $hasil_g01 . ' + ' . $hasil_g04 . ' * (1 - ' . $hasil_g01 . ')',
                'Proc 2 (Proc 1 & G05) = ' . $proc1_bronkitis . ' + ' . $hasil_g05 . ' * (1 - ' . $proc1_bronkitis . ')',
                'Proc 3 (Proc 2 & G06) = ' . $proc2_bronkitis . ' + ' . $hasil_g06 . ' * (1 - ' . $proc2_bronkitis . ')',
                'Proc 4 (Proc 3 & G07) = ' . $proc3_bronkitis . ' + ' . $hasil_g07 . ' * (1 - ' . $proc3_bronkitis . ')',
                'Proc 5 (Proc 4 & G08) = ' . $proc4_bronkitis . ' + ' . $hasil_g08 . ' * (1 - ' . $proc4_bronkitis . ')',
                'Proc 6 (Proc 5 & G09) = ' . $proc5_bronkitis . ' + ' . $hasil_g09 . ' * (1 - ' . $proc5_bronkitis . ')',
                'Proc 7 (Proc 6 & G10) = ' . $proc6_bronkitis . ' + ' . $hasil_g10 . ' * (1 - ' . $proc6_bronkitis . ')',
                'Hasil = ' . $hasil_bronkitis
            ],
            'langkah_tuberkolosis_paru' => [
                'Proc 1 (G03 & G04)    = ' . $hasil_g03 . ' + ' . $hasil_g04 . ' * (1 - ' . $hasil_g03 . ')',
                'Proc 2 (Proc 1 & G05) = ' . $proc1_tuberkolosis_paru . ' + ' . $hasil_g05 . ' * (1 - ' . $proc1_tuberkolosis_paru . ')',
                'Proc 3 (Proc 2 & G06) = ' . $proc2_tuberkolosis_paru . ' + ' . $hasil_g06 . ' * (1 - ' . $proc2_tuberkolosis_paru . ')',
                'Proc 4 (Proc 3 & G12) = ' . $proc3_tuberkolosis_paru . ' + ' . $hasil_g12 . ' * (1 - ' . $proc3_tuberkolosis_paru . ')',
                'Proc 5 (Proc 4 & G13) = ' . $proc4_tuberkolosis_paru . ' + ' . $hasil_g13 . ' * (1 - ' . $proc4_tuberkolosis_paru . ')',
                'Proc 6 (Proc 5 & G14) = ' . $proc5_tuberkolosis_paru . ' + ' . $hasil_g14 . ' * (1 - ' . $proc5_tuberkolosis_paru . ')',
                'Proc 7 (Proc 6 & G15) = ' . $proc6_tuberkolosis_paru . ' + ' . $hasil_g15 . ' * (1 - ' . $proc6_tuberkolosis_paru . ')',
                'Proc 8 (Proc 7 & G16) = ' . $proc7_tuberkolosis_paru . ' + ' . $hasil_g16 . ' * (1 - ' . $proc7_tuberkolosis_paru . ')',
                'Total = ' . $hasil_tuberkolosis_paru
            ],
            'langkah_emfisema' => [
                'Proc 1 (G01 & G03)    = ' . $hasil_g01 . ' + ' . $hasil_g03 . ' * (1 - ' . $hasil_g01 . ')',
                'Proc 2 (Proc 1 & G04) = ' . $proc1_emfisema . ' + ' . $hasil_g04 . ' * (1 - ' . $proc1_emfisema . ')',
                'Proc 3 (Proc 2 & G10) = ' . $proc2_emfisema . ' + ' . $hasil_g10 . ' * (1 - ' . $proc2_emfisema . ')',
                'Proc 4 (Proc 3 & G17) = ' . $proc3_emfisema . ' + ' . $hasil_g17 . ' * (1 - ' . $proc3_emfisema . ')',
                'Proc 5 (Proc 4 & G18) = ' . $proc4_emfisema . ' + ' . $hasil_g18 . ' * (1 - ' . $proc4_emfisema . ')',
                'Hasil = ' . $hasil_emfisema
            ],
            'langkah_abses_paru' => [
                'Proc 1 (G02 & G19)    = ' . $hasil_g02 . ' + ' . $hasil_g19 . ' * (1 - ' . $hasil_g02 . ')',
                'Proc 2 (Proc 1 & G20) = ' . $proc1_abses_paru . ' + ' . $hasil_g20 . ' * (1 - ' . $proc1_abses_paru . ')',
                'Proc 3 (Proc 2 & G21) = ' . $proc2_abses_paru . ' + ' . $hasil_g21 . ' * (1 - ' . $proc2_abses_paru . ')',
                'Hasil = ' . $hasil_abses_paru
            ]
        ];

        $data_langkah_cf_user_cf_pakar = implode('<br>', $data_langkah['cf_user_cf_pakar']);
        $data_langkah_pneumonia = implode('<br>', $data_langkah['langkah_pneumonia']);
        $data_langkah_bronkitis = implode('<br>', $data_langkah['langkah_bronkitis']);
        $data_langkah_tuberkolosis_paru = implode('<br>', $data_langkah['langkah_tuberkolosis_paru']);
        $data_langkah_emfisema = implode('<br>', $data_langkah['langkah_emfisema']);
        $data_langkah_abses_paru = implode('<br>', $data_langkah['langkah_abses_paru']);

        return view('hasil-tes.detail', ['data' => $data, 'edukasi' => $edukasi, 'data_langkah_cf_user_cf_pakar' => $data_langkah_cf_user_cf_pakar, 'data_langkah_pneumonia' => $data_langkah_pneumonia, 'data_langkah_bronkitis' => $data_langkah_bronkitis, 'data_langkah_tuberkolosis_paru' => $data_langkah_tuberkolosis_paru, 'data_langkah_emfisema' => $data_langkah_emfisema, 'data_langkah_abses_paru' => $data_langkah_abses_paru]);
    }

    public function detail_jawaban($id)
    {
        $data = Test_gejala::find($id);

        return view('hasil-tes.detailJawaban', ['data' => $data]);
    }

    public function save(Request $request)
    {

        $request->validate([
            'g01' => 'required',
            'g02' => 'required',
            'g03' => 'required',
            'g05' => 'required',
            'g06' => 'required',
            'g07' => 'required',
            'g08' => 'required',
            'g09' => 'required',
            'g10' => 'required',
            'g11' => 'required',
            'g12' => 'required',
            'g13' => 'required',
            'g14' => 'required',
            'g15' => 'required',
            'g16' => 'required',
            'g17' => 'required',
            'g18' => 'required',
            'g19' => 'required',
            'g20' => 'required',
            'g21' => 'required',
        ]);

        $user_id = Auth::id();

        $data = new Test_gejala;
        $hasil_diagnosa = new Hasil_diagnosa;

        $cf_g01 = 0.5;
        $cf_g02 = 0.8;
        $cf_g03 = 0.6;
        $cf_g04 = 0.6;
        $cf_g05 = 0.6;
        $cf_g06 = 0.8;
        $cf_g07 = 0.8;
        $cf_g08 = 0.8;
        $cf_g09 = 0.6;
        $cf_g10 = 0.6;
        $cf_g11 = 1;
        $cf_g12 = 0.5;
        $cf_g13 = 0.6;
        $cf_g14 = 0.6;
        $cf_g15 = 0.8;
        $cf_g16 = 1;
        $cf_g17 = 0.8;
        $cf_g18 = 1;
        $cf_g19 = 0.8;
        $cf_g20 = 1;
        $cf_g21 = 0.8;

        $data->g01 = $request->g01;
        $data->g02 = $request->g02;
        $data->g03 = $request->g03;
        $data->g04 = $request->g04;
        $data->g05 = $request->g05;
        $data->g06 = $request->g06;
        $data->g07 = $request->g07;
        $data->g08 = $request->g08;
        $data->g09 = $request->g09;
        $data->g10 = $request->g10;
        $data->g11 = $request->g11;
        $data->g12 = $request->g12;
        $data->g13 = $request->g13;
        $data->g14 = $request->g14;
        $data->g15 = $request->g15;
        $data->g16 = $request->g16;
        $data->g17 = $request->g17;
        $data->g18 = $request->g18;
        $data->g19 = $request->g19;
        $data->g20 = $request->g20;
        $data->g21 = $request->g21;
        $data->user_id = $user_id;

        $data->save();

        $nilai_pneumonia = $this->pneumonia($data->g01, $data->g02, $data->g03, $data->g04, $data->g11, $cf_g01, $cf_g02, $cf_g03, $cf_g04, $cf_g11);
        $nilai_bronkitis = $this->bronkitis($data->g01, $data->g05, $data->g04, $data->g06, $data->g07, $data->g08, $data->g09, $data->g10, $cf_g01, $cf_g05, $cf_g04, $cf_g06, $cf_g07, $cf_g08, $cf_g09, $cf_g10,);
        $nilai_tuberkulosis = $this->tuberkolosis_paru($data->g03, $data->g04, $data->g05, $data->g06, $data->g12, $data->g13, $data->g14, $data->g15, $data->g16, $cf_g03, $cf_g04, $cf_g05, $cf_g06, $cf_g12, $cf_g13, $cf_g14, $cf_g15, $cf_g16);
        $nilai_emfisema = $this->emfisema($data->g01, $data->g03, $data->g04, $data->g10, $data->g17, $data->g18, $cf_g01, $cf_g03, $cf_g04, $cf_g10, $cf_g17, $cf_g18);
        $nilai_abses = $this->abses_paru($data->g02, $data->g19, $data->g20, $data->g21, $cf_g02, $cf_g19, $cf_g20, $cf_g21);

        $edukasi_pneumonia = [
            "Vaksinasi (vaksin pneumokokal dan vaksin influenza)",
            "Berhenti merokok",
            "Menjaga kebersihan tangan, penggunaan masker, menerapkan etika batuk",
            "Menerapkan kewaspadaan standar dan isolasi",
            "segera periksakan diri ke rumah sakit atau ke BKPM Purwokerto",
        ];
        $edukasi_pneumonia_json = json_encode($edukasi_pneumonia);

        $edukasi_bronkitis = [
            "Melaksanakan aktivitas sehari-hari sesuai dengan pola hidup yang sehat.",
            "Berhenti merokok, menghindari asap rokok, mengontrol suhu, dan kelembapan lingkungan, nutrisi yang baik dan cairan yang adekuat.",
            "Gejala efek samping obat, seperti bronkidilator, dapat menimbulkan berdebar, lemas, gemetar, dan keringat dingin.",
            "Segera periksakan diri ke rumah sakit atau ke BKPM Purwokerto."
        ];
        $edukasi_bronkitis_json = json_encode($edukasi_bronkitis);

        $edukasi_tuberkulosis = [
            "Tidak boleh putus obat, memakai masker",
            "Etika batuk",
            "Pola hidup yang sehat",
            "Asupan gizi yang baik",
            "Segera periksakan diri ke rumah sakit atau ke BKPM Purwokerto"
        ];
        $edukasi_tuberkulosis_json = json_encode($edukasi_tuberkulosis);

        $edukasi_emfisema = [
            "Kemungkinan komplikasi",
            "Pola hidup bersih dan sehat",
            "Segera periksakan diri ke rumah sakit atau ke BKPM Purwokerto"
        ];
        $edukasi_emfisema_json = json_encode($edukasi_emfisema);

        $edukasi_abses = [
            "Pola hidup bersih dan sehat",
            "Asupan gizi yang baik",
            "Segera periksakan diri ke rumah sakit atau ke BKPM Purwokerto",
        ];
        $edukasi_abses_json = json_encode($edukasi_abses);


        $penyakit = [
            'pneumonia' => $nilai_pneumonia,
            'bronkitis' => $nilai_bronkitis,
            'tuberkulosis' => $nilai_tuberkulosis,
            'emfisema' => $nilai_emfisema,
            'abses' => $nilai_abses,
        ];



        // Menemukan penyakit dengan nilai terbesar
        $penyakit_terbesar = array_keys($penyakit, max($penyakit))[0];

        // Menampilkan hasil
        if ($penyakit_terbesar == 'pneumonia') {
            $hasil_diagnosa->nama_penyakit = "Pneumonia";
            $hasil_diagnosa->diagnosa_banding = "Tumor Paru, Mikosis, Efusi Pleura";
            $hasil_diagnosa->edukasi = $edukasi_pneumonia_json;
            $hasil_diagnosa->test_gejala_id = $data->id;
            $hasil_diagnosa->save();
        } elseif ($penyakit_terbesar == 'bronkitis') {
            $hasil_diagnosa->nama_penyakit = "Bronkitis";
            $hasil_diagnosa->diagnosa_banding = "Epiglotitis, Bronkiolitis, Influenza, Simusitis, Ppok, Farangitis, Asma, Bronkiektasis";
            $hasil_diagnosa->edukasi = $edukasi_bronkitis_json;
            $hasil_diagnosa->test_gejala_id = $data->id;
            $hasil_diagnosa->save();
        } elseif ($penyakit_terbesar == 'tuberkulosis') {
            $hasil_diagnosa->nama_penyakit = "Tuberkolosis Paru";
            $hasil_diagnosa->diagnosa_banding = "Pneumonia Komunitas, Bronkiestasis, Mikosis Paru, Tumor Paru";
            $hasil_diagnosa->edukasi = $edukasi_tuberkulosis_json;
            $hasil_diagnosa->test_gejala_id = $data->id;
            $hasil_diagnosa->save();
        } elseif ($penyakit_terbesar == 'emfisema') {
            $hasil_diagnosa->nama_penyakit = "Emfisema";
            $hasil_diagnosa->diagnosa_banding = "Mediastinitis, Pneumotoraks, ARDS, Sindrom Aspirasi";
            $hasil_diagnosa->edukasi = $edukasi_emfisema_json;
            $hasil_diagnosa->test_gejala_id = $data->id;
            $hasil_diagnosa->save();
        } elseif ($penyakit_terbesar == 'abses') {
            $hasil_diagnosa->nama_penyakit = "Abses Paru";
            $hasil_diagnosa->diagnosa_banding = "Bronkiektasis, Bula, Infark Paru, Kanker Paru";
            $hasil_diagnosa->edukasi = $edukasi_abses_json;
            $hasil_diagnosa->test_gejala_id = $data->id;
            $hasil_diagnosa->save();
        }

        return redirect('/tes-kesehatan')->with('success', 'Hasil Tes Kesehatan Anda berhasil direkam. Silakan cek menu Hasil Tes Kesehatan untuk melihat detail tes Anda.');
    }

    private function pneumonia($g01, $g02, $g03, $g04, $g11, $cfg01, $cfg02, $cfg03, $cfg04, $cfg11)
    {
        $hasil_g01 = $g01 * $cfg01;
        $hasil_g02 = $g02 * $cfg02;
        $hasil_g03 = $g03 * $cfg03;
        $hasil_g04 = $g04 * $cfg04;
        $hasil_g11 = $g11 * $cfg11;

        $proc1 = $hasil_g01 + $hasil_g02 * (1 - $hasil_g01);
        $proc2 = $proc1 + $hasil_g03 * (1 - $proc1);
        $proc3 = $proc2 + $hasil_g04 * (1 - $proc2);
        $hasil = $proc3 + $hasil_g11 * (1 - $proc3);

        return $hasil;
    }

    private function bronkitis($g01, $g05, $g04, $g06, $g07, $g08, $g09, $g10, $cfg01, $cfg05, $cfg04, $cfg06, $cfg07, $cfg08, $cfg09, $cfg10,)
    {
        $hasil_g01 = $g01 * $cfg01;
        $hasil_g05 = $g05 * $cfg05;
        $hasil_g04 = $g04 * $cfg04;
        $hasil_g06 = $g06 * $cfg06;
        $hasil_g07 = $g07 * $cfg07;
        $hasil_g08 = $g08 * $cfg08;
        $hasil_g09 = $g09 * $cfg09;
        $hasil_g10 = $g10 * $cfg10;

        $proc1 = $hasil_g01 + $hasil_g05 * (1 - $hasil_g01);
        $proc2 = $proc1 + $hasil_g04 * (1 - $proc1);
        $proc3 = $proc2 + $hasil_g06 * (1 - $proc2);
        $proc4 = $proc3 + $hasil_g07 * (1 - $proc3);
        $proc5 = $proc4 + $hasil_g08 * (1 - $proc4);
        $proc6 = $proc5 + $hasil_g09 * (1 - $proc5);
        $hasil = $proc6 + $hasil_g10 * (1 - $proc6);

        return $hasil;
    }

    private function tuberkolosis_paru($g03, $g04, $g05, $g06, $g12, $g13, $g14, $g15, $g16, $cfg03, $cfg04, $cfg05, $cfg06, $cfg12, $cfg13, $cfg14, $cfg15, $cfg16)
    {
        $hasil_g03 = $g03 * $cfg03;
        $hasil_g04 = $g04 * $cfg04;
        $hasil_g05 = $g05 * $cfg05;
        $hasil_g06 = $g06 * $cfg06;
        $hasil_g12 = $g12 * $cfg12;
        $hasil_g13 = $g13 * $cfg13;
        $hasil_g14 = $g14 * $cfg14;
        $hasil_g15 = $g15 * $cfg15;
        $hasil_g16 = $g16 * $cfg16;

        $proc1 = $hasil_g03 + $hasil_g04 * (1 - $hasil_g03);
        $proc2 = $proc1 + $hasil_g05 * (1 - $proc1);
        $proc3 = $proc2 + $hasil_g06 * (1 - $proc2);
        $proc4 = $proc3 + $hasil_g12 * (1 - $proc3);
        $proc5 = $proc4 + $hasil_g13 * (1 - $proc4);
        $proc6 = $proc5 + $hasil_g14 * (1 - $proc5);
        $proc7 = $proc6 + $hasil_g15 * (1 - $proc6);
        $hasil = $proc7 + $hasil_g16 * (1 - $proc7);

        return $hasil;
    }

    private function emfisema($g01, $g03, $g04, $g10, $g17, $g18, $cfg01, $cfg03, $cfg04, $cfg10, $cfg17, $cfg18)
    {
        $hasil_g01 = $g01 * $cfg01;
        $hasil_g03 = $g03 * $cfg03;
        $hasil_g04 = $g04 * $cfg04;
        $hasil_g10 = $g10 * $cfg10;
        $hasil_g17 = $g17 * $cfg17;
        $hasil_g18 = $g18 * $cfg18;

        $proc1 = $hasil_g01 + $hasil_g03 * (1 - $hasil_g01);
        $proc2 = $proc1 + $hasil_g04 * (1 - $proc1);
        $proc3 = $proc2 + $hasil_g10 * (1 - $proc2);
        $proc4 = $proc3 + $hasil_g17 * (1 - $proc3);
        $hasil = $proc4 + $hasil_g18 * (1 - $proc4);

        return $hasil;
    }

    private function abses_paru($g02, $g19, $g20, $g21, $cfg02, $cfg19, $cfg20, $cfg21)
    {
        $hasil_g02 = $g02 * $cfg02;
        $hasil_g19 = $g19 * $cfg19;
        $hasil_g20 = $g20 * $cfg20;
        $hasil_g21 = $g21 * $cfg21;

        $proc1 = $hasil_g02 + $hasil_g19 * (1 - $hasil_g02);
        $proc2 = $proc1 + $hasil_g20 * (1 - $proc1);
        $hasil = $proc2 + $hasil_g21 * (1 - $proc2);

        return $hasil;
    }
}
