<?php

namespace App\Http\Controllers;

use App\Models\Hasil_diagnosa;
use App\Models\Test_gejala;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class welcomeController extends Controller
{
    public function index()
    {

        $totalUsers = User::count();
        $totalDiagnosa = Hasil_diagnosa::count();
        $data = new \stdClass();
        $data->pria_pneumonia = 0;
        $data->pria_bronkitis = 0;
        $data->pria_tuberkolosis_paru = 0;
        $data->pria_emfisema = 0;
        $data->pria_abses_paru = 0;
        $data->wanita_pneumonia = 0;
        $data->wanita_bronkitis = 0;
        $data->wanita_tuberkolosis_paru = 0;
        $data->wanita_emfisema = 0;
        $data->wanita_abses_paru = 0;

        if ($totalDiagnosa != 0) {
            for ($i = 1; $i <= $totalUsers; $i++) {
                $user = User::find($i);
                $test = Test_gejala::where('user_id', $user->id)->latest('created_at')->first();
                if ($test != null) {
                    $diagnosa = Hasil_diagnosa::where('test_gejala_id', $test->id)->first();
                    $diseaseKey = strtolower(str_replace(' ', '_', $diagnosa->nama_penyakit));
                    $variableName = $user->gender . '_' . $diseaseKey;
                    $data->$variableName = +1;
                }
            }
        }


        $max = max(
            $data->pria_pneumonia,
            $data->pria_bronkitis,
            $data->pria_tuberkolosis_paru,
            $data->pria_emfisema,
            $data->pria_abses_paru,
            $data->wanita_pneumonia,
            $data->wanita_bronkitis,
            $data->wanita_tuberkolosis_paru,
            $data->wanita_emfisema,
            $data->wanita_abses_paru,
        ) + 2;

        return view('welcome', ['data' => $data, 'max' => $max]);
    }
}
