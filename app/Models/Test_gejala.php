<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test_gejala extends Model
{
    use HasFactory;

    protected $table = 'test_gejala';
    protected $fillable = ['g01', 'g02','g03','g04','g05','g06','g07','g08','g09','g10','g11','g12','g13','g14','g15','g16','g17','g18','g19','g20','g21', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function hasil_diagnosa()
    {
        return $this->hasOne(Hasil_diagnosa::class, 'test_gejala_id', 'id',);
    }
}
