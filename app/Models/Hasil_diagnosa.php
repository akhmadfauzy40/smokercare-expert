<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasil_diagnosa extends Model
{
    use HasFactory;

    protected $table = 'hasil_diagnosa';
    protected $fillable = ['nama_penyakit', 'diagnosa_banding', 'cara_penanganan', 'user_id', 'test_gejala_id'];

    public function test_gejala()
    {
        return $this->belongsTo(Test_gejala::class, 'test_gejala_id', 'id');
    }
}
