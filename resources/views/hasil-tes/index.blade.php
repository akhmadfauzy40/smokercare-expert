@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css">
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Hasil Tes Kesehatan</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Sample Data -->

                        <!-- Table Header -->
                        <table id="hasil" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Tes</th>
                                    <th>Diagnosa Penyakit</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Populate Data -->
                                @foreach ($data as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>{{ $item->created_at->format('d-m-Y') }}<br>{{ $item->created_at->format('H:i:s') }}</td>
                                        <td>{{ $item->hasil_diagnosa->nama_penyakit }}</td>
                                        <td>
                                            <center><a href="/detail/{{ $item->hasil_diagnosa->id }}" class="btn btn-info">Selengkapnya</a></center>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="https://www.instagram.com/sonyaputriily"
                    target="_blank" class="pe-1 text-primary text-decoration-underline">Sonya</a></p>
        </div>
        <!-- end row -->
    </div>
    <!-- container-fluid -->
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>
    <script>
        new DataTable('#hasil');
    </script>
@endpush
