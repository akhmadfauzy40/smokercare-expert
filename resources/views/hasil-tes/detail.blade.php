@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Hasil Tes Kesehatan</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Detail Hasil Tes Kesehatan</h5>
                        <p><strong>Di Duga Terkena :</strong> {{ $data->nama_penyakit }}</p>
                        <p><strong>Diagnosa Banding :</strong> {{ $data->diagnosa_banding }}</p>
                        <p><strong>Edukasi :</strong></p>
                        @foreach ($edukasi as $no => $item)
                            {{ $no + 1 }}.&nbsp;{{ $item }}<br>
                        @endforeach

                        <div class="text-end">
                            <a href="/detail-test/{{ $data->test_gejala->id }}" class="btn btn-primary">Lihat Detail Jawaban Test</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Langkah Melakukan Diagnosa</h5>
                        <p>Untuk melakukan diagnosa, langkah pertama yaitu menghitung CF_user * CF_pakar terlebih dahulu (CF_pakar sudah di tentukan dari sistem sedangkan CF_user adalah hasil dari jawab user ketika melakukan test kesehatan) menggunakan rumus :
                            <pre style="font-size: 15px">Total_Gxx = CF_user * CF_pakar = N</pre>
                        </p>
                        <p><strong>Hasil Perhitungan CF_user * CF_pakar:</strong><br>
                        <pre style="font-size: 15px">{!! $data_langkah_cf_user_cf_pakar !!}</pre></p>
                        <p>Setelah menghitung CF_user * CF_pakar langkah selanjutnya adalah melakukan perhitungan di setiap gejala</p>
                        <p><strong>Perhitungan pada gejala Pneumonia (G01,G02,G03,G04,G11) : </strong><br>
                        <pre style="font-size: 15px">{!! $data_langkah_pneumonia !!}</pre></p>
                        <p><strong>Perhitungan pada gejala Bronkitis (G01, G04, G05, G06, G07, G08, G09, G10) : </strong><br>
                        <pre style="font-size: 15px">{!! $data_langkah_bronkitis !!}</pre></p>
                        <p><strong>Perhitungan pada gejala Tuberkolosis Paru (G03, G04, G05, G06, G12, G13, G14, G15, G16) : </strong><br>
                        <pre style="font-size: 15px">{!! $data_langkah_tuberkolosis_paru !!}</pre></p>
                        <p><strong>Perhitungan pada gejala Emfisema (G01, G03, G04, G10, G17, G18) : </strong><br>
                        <pre style="font-size: 15px">{!! $data_langkah_emfisema !!}</pre></p>
                        <p><strong>Perhitungan pada gejala Abses Paru (G02, G19, G20, G21) : </strong><br>
                        <pre style="font-size: 15px">{!! $data_langkah_abses_paru !!}</pre></p>
                        <p>Setelah menghitung seluruh gejala penyakit, langkah selanjutnya adalah mengambil nilai terbesar dari perhitungan gejala penyakit. Dari kasus saat ini penyakit <strong>{{ $data->nama_penyakit }}</strong> adalah nilai terbesar pada hasil perhitungan di setiap gejala oleh karena itu user di duga terkena penyakit <strong>{{ $data->nama_penyakit }}</strong></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="https://www.instagram.com/sonyaputriily"
                    target="_blank" class="pe-1 text-primary text-decoration-underline">Sonya</a></p>
        </div>
    </div>
@endsection
