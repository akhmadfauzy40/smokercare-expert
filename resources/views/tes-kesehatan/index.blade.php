@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Tes Kesehatan</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Selamat datang di Tes Kesehatan!</h4>
                        <p class="card-text">Tes kesehatan ini membantu Anda untuk mengevaluasi kondisi paru-paru Anda.
                            Silakan klik tombol di bawah untuk memulai tes.</p>
                        <a href="/start-test" class="btn btn-primary">Mulai Tes Kesehatan</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="https://www.instagram.com/sonyaputriily"
                    target="_blank" class="pe-1 text-primary text-decoration-underline">Sonya</a></p>
        </div>
    </div>
    <!-- container-fluid -->
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/swal.min.js') }}"></script>
    @if (session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: "{{ session('success') }}",
                showConfirmButton: true // Tidak menampilkan tombol OK
            });
        </script>
    @endif
@endpush
