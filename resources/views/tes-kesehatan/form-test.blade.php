@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Tes Kesehatan</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <form method="post" action="/submit-health-test">
                    @csrf

                    <!-- Pertanyaan g01 -->
                    <div class="card" id="g01-card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Batuk?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g01-yes" autocomplete="off"
                                    onclick="enableAdditional('g01-additional', 'g01-no')">
                                <label class="btn btn-outline-danger" for="g01-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g01" id="g01-no" value="0"
                                    autocomplete="off" onclick="disableAdditional('g01-additional', 'g01-yes')">
                                <label class="btn btn-outline-success" for="g01-no">Tidak</label>
                            </div>

                            <div id="g01-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g01" id="g01_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g01_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>


                    <!-- Pertanyaan g02 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Demam Lebih dari 38 Derajat
                                Celcius?
                            </h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g02-yes" autocomplete="off"
                                    onclick="enableAdditional('g02-additional', 'g02-no')">
                                <label class="btn btn-outline-danger" for="g02-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g02" id="g02-no" value="0"
                                    autocomplete="off" onclick="disableAdditional('g02-additional', 'g02-yes')">
                                <label class="btn btn-outline-success" for="g02-no">Tidak</label>
                            </div>

                            <div id="g02-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g02" id="g02_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g02_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g03 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Nyeri di Dada?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g03-yes" autocomplete="off"
                                    onclick="enableAdditional('g03-additional', 'g03-no')">
                                <label class="btn btn-outline-danger" for="g03-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g03" id="g03-no" value="0"
                                    autocomplete="off" onclick="disableAdditional('g03-additional', 'g03-yes')">
                                <label class="btn btn-outline-success" for="g03-no">Tidak</label>
                            </div>

                            <div id="g03-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g03" id="g03_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g03_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g04 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Sesak Nafas?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g04-yes" autocomplete="off"
                                    onclick="enableAdditional('g04-additional', 'g04-no')">
                                <label class="btn btn-outline-danger" for="g04-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g04" id="g04-no" value="0"
                                    autocomplete="off" onclick="disableAdditional('g04-additional', 'g04-yes')">
                                <label class="btn btn-outline-success" for="g04-no">Tidak</label>
                            </div>

                            <div id="g04-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g04" id="g04_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g04_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g05 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Batuk Berdahak?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check"id="g05-yes" autocomplete="off"
                                    onclick="enableAdditional('g05-additional', 'g05-no')">
                                <label class="btn btn-outline-danger" for="g05-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g05" id="g05-no" value="0"
                                    autocomplete="off" onclick="disableAdditional('g05-additional', 'g05-yes')">
                                <label class="btn btn-outline-success" for="g05-no">Tidak</label>
                            </div>

                            <div id="g05-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g05" id="g05_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g05_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g06 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Batuk Berdarah?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g06-yes" autocomplete="off"
                                    onclick="enableAdditional('g06-additional', 'g06-no')">
                                <label class="btn btn-outline-danger" for="g06-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g06" value="0" id="g06-no"
                                    autocomplete="off" onclick="disableAdditional('g06-additional', 'g06-yes')">
                                <label class="btn btn-outline-success" for="g06-no">Tidak</label>
                            </div>

                            <div id="g06-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g06" id="g06_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g06_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g07 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Mengi?</h5>
                            <p class="card-text">* Mengi adalah kondisi terjadinya suara mendesing atau bersiul pada saat
                                bernafas, umumnya terdengar saat menghela napas.</p>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g07-yes" autocomplete="off"
                                    onclick="enableAdditional('g07-additional', 'g07-no')">
                                <label class="btn btn-outline-danger" for="g07-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g07" value="0" id="g07-no"
                                    autocomplete="off" onclick="disableAdditional('g07-additional', 'g07-yes')">
                                <label class="btn btn-outline-success" for="g07-no">Tidak</label>
                            </div>

                            <div id="g07-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g07" id="g07_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g07_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g08 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Hidung Tersumbat?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g08-yes" autocomplete="off"
                                    onclick="enableAdditional('g08-additional', 'g08-no')">
                                <label class="btn btn-outline-danger" for="g08-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g08" value="0" id="g08-no"
                                    autocomplete="off" onclick="disableAdditional('g08-additional', 'g08-yes')">
                                <label class="btn btn-outline-success" for="g08-no">Tidak</label>
                            </div>

                            <div id="g08-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g08" id="g08_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g08_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g09 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Nyeri Tenggorokan?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g09-yes" autocomplete="off"
                                    onclick="enableAdditional('g09-additional', 'g09-no')">
                                <label class="btn btn-outline-danger" for="g09-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g09" value="0" id="g09-no"
                                    autocomplete="off" onclick="disableAdditional('g09-additional', 'g09-yes')">
                                <label class="btn btn-outline-success" for="g09-no">Tidak</label>
                            </div>

                            <div id="g09-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g09" id="g09_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g09_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g10 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Demam Ringan?
                            </h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g10-yes" autocomplete="off"
                                    onclick="enableAdditional('g10-additional', 'g10-no')">
                                <label class="btn btn-outline-danger" for="g10-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g10" value="0" id="g10-no"
                                    autocomplete="off" onclick="disableAdditional('g10-additional', 'g10-yes')">
                                <label class="btn btn-outline-success" for="g10-no">Tidak</label>
                            </div>

                            <div id="g10-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g10" id="g10_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g10_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g11 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Perubahan Sputum atau Purulent?
                            </h5>
                            <p class="card-text">* Perubahan sputum atau purulen merujuk pada perubahan warna atau
                                konsistensi dahak yang dikeluarkan saat batuk, yang bisa menjadi tanda adanya infeksi atau
                                kondisi kesehatan lainnya.</p>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g11-yes" autocomplete="off"
                                    onclick="enableAdditional('g11-additional', 'g11-no')">
                                <label class="btn btn-outline-danger" for="g11-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g11" value="0" id="g11-no"
                                    autocomplete="off" onclick="disableAdditional('g11-additional', 'g11-yes')">
                                <label class="btn btn-outline-success" for="g11-no">Tidak</label>
                            </div>

                            <div id="g11-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf3" value="0.3"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf4" value="0.4"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf5" value="0.5"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf6" value="0.6"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf7" value="0.7"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf8" value="0.8"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g11" id="g11_cf9" value="1"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g11_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g12 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Badan Lemas?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g12-yes" autocomplete="off"
                                    onclick="enableAdditional('g12-additional', 'g12-no')">
                                <label class="btn btn-outline-danger" for="g12-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g12" value="0" id="g12-no"
                                    autocomplete="off" onclick="disableAdditional('g12-additional', 'g12-yes')">
                                <label class="btn btn-outline-success" for="g12-no">Tidak</label>
                            </div>

                            <div id="g12-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf2" value="0.2"
                                    autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g12" id="g12_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g12_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g13 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Penurunan Nafsu Makan?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g13-yes" autocomplete="off"
                                    onclick="enableAdditional('g13-additional', 'g13-no')">
                                <label class="btn btn-outline-danger" for="g13-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g13" value="0"
                                    id="g13-no" autocomplete="off" onclick="disableAdditional('g13-additional', 'g13-yes')">
                                <label class="btn btn-outline-success" for="g13-no">Tidak</label>
                            </div>

                            <div id="g13-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g13" id="g13_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g13_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g14 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Penurunan Berat Badan?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g14-yes" autocomplete="off"
                                    onclick="enableAdditional('g14-additional', 'g14-no')">
                                <label class="btn btn-outline-danger" for="g14-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g14" value="0"
                                    id="g14-no" autocomplete="off" onclick="disableAdditional('g14-additional', 'g14-yes')">
                                <label class="btn btn-outline-success" for="g14-no">Tidak</label>
                            </div>

                            <div id="g14-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g14" id="g14_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g14_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g15 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Berkeringat Malam Hari?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g15-yes" autocomplete="off"
                                    onclick="enableAdditional('g15-additional', 'g15-no')">
                                <label class="btn btn-outline-danger" for="g15-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g15" value="0"
                                    id="g15-no" autocomplete="off" onclick="disableAdditional('g15-additional', 'g15-yes')">
                                <label class="btn btn-outline-success" for="g15-no">Tidak</label>
                            </div>

                            <div id="g15-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g15" id="g15_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g15_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g16 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Demam Subfebris?</h5>
                            <p class="card-text">* Demam subfebris adalah kondisi demam ringan atau suhu tubuh yang
                                sedikit
                                di atas suhu normal, tetapi masih di bawah 38 derajat Celsius. Gejala ini bisa menjadi tanda
                                awal suatu penyakit atau infeksi ringan.</p>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g16-yes" autocomplete="off"
                                    onclick="enableAdditional('g16-additional', 'g16-no')">
                                <label class="btn btn-outline-danger" for="g16-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g16" value="0"
                                    id="g16-no" autocomplete="off" onclick="disableAdditional('g16-additional', 'g16-yes')">
                                <label class="btn btn-outline-success" for="g16-no">Tidak</label>
                            </div>

                            <div id="g16-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g16" id="g16_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g16_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g17 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Nyeri Restrosternal?</h5>
                            <p class="card-text">* Nyeri restrosternal merujuk pada rasa nyeri atau ketidaknyamanan di
                                area
                                tengah dada, yang bisa terjadi di belakang tulang dada atau di bawah sternum. Kondisi ini
                                seringkali terkait dengan gangguan pada saluran pencernaan atau jantung.</p>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g17-yes" autocomplete="off"
                                    onclick="enableAdditional('g17-additional', 'g17-no')">
                                <label class="btn btn-outline-danger" for="g17-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g17" value="0"
                                    id="g17-no" autocomplete="off" onclick="disableAdditional('g17-additional', 'g17-yes')">
                                <label class="btn btn-outline-success" for="g17-no">Tidak</label>
                            </div>

                            <div id="g17-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g17" id="g17_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g17_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g18 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Distagia & Distonia?</h5>
                            <p class="card-text">* Distagia adalah kesulitan atau rasa tidak nyaman saat menelan makanan
                                atau minuman. Sementara distonia merujuk pada kelainan gerakan otot yang dapat menyebabkan
                                kontraksi otot yang tidak terkendali. Kombinasi keduanya dapat mempengaruhi fungsi menelan.
                            </p>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g18-yes" autocomplete="off"
                                    onclick="enableAdditional('g18-additional', 'g18-no')">
                                <label class="btn btn-outline-danger" for="g18-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g18" value="0"
                                    id="g18-no" autocomplete="off" onclick="disableAdditional('g18-additional', 'g18-yes')">
                                <label class="btn btn-outline-success" for="g18-no">Tidak</label>
                            </div>

                            <div id="g18-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g18" id="g18_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g18_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g19 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Jari Tabuh?</h5>
                            <p class="card-text">* Jari tabuh adalah kondisi di mana ujung jari tangan atau kaki mengalami
                                pembengkakan yang menyebabkan ujungnya menyerupai bentuk tabuh. Ini bisa menjadi gejala dari
                                beberapa kondisi medis, seperti arthritis atau penyakit jaringan ikat.</p>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g19-yes" autocomplete="off"
                                    onclick="enableAdditional('g19-additional', 'g19-no')">
                                <label class="btn btn-outline-danger" for="g19-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g19" value="0"
                                    id="g19-no" autocomplete="off" onclick="disableAdditional('g19-additional', 'g19-yes')">
                                <label class="btn btn-outline-success" for="g19-no">Tidak</label>
                            </div>

                            <div id="g19-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g19" id="g19_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g19_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g20 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Kakeksia?</h5>
                            <p class="card-text">* Kakeksia adalah keadaan kekurangan gizi yang ditandai oleh penurunan
                                berat badan yang signifikan, kelemahan otot, dan kehilangan massa jaringan tubuh secara
                                umum. Ini sering terkait dengan penyakit kronis, seperti kanker atau HIV/AIDS.</p>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g20-yes" autocomplete="off"
                                    onclick="enableAdditional('g20-additional', 'g20-no')">
                                <label class="btn btn-outline-danger" for="g20-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g20" value="0"
                                    id="g20-no" autocomplete="off" onclick="disableAdditional('g20-additional', 'g20-yes')">
                                <label class="btn btn-outline-success" for="g20-no">Tidak</label>
                            </div>

                            <div id="g20-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g20" id="g20_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g20_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <!-- Pertanyaan g21 -->
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title d-inline-block">Apakah Anda Mengalami Penurunan Suara Nafas?</h5>

                            <div class="float-end" data-toggle="buttons">
                                <input type="radio" class="btn-check" id="g21-yes" autocomplete="off"
                                    onclick="enableAdditional('g21-additional', 'g21-no')">
                                <label class="btn btn-outline-danger" for="g21-yes">Ya</label>

                                <input type="radio" class="btn-check" name="g21" value="0"
                                    id="g21-no" autocomplete="off" onclick="disableAdditional('g21-additional', 'g21-yes')">
                                <label class="btn btn-outline-success" for="g21-no">Tidak</label>
                            </div>

                            <div id="g21-additional" style="margin-top: 10px; display: none;">
                                <h6>Pilih Tingkat Keyakinan:</h6>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf2"
                                    value="0.2" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf2">Hampir Tidak Pasti</label>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf3"
                                    value="0.3" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf3">Kemungkinan Besar Tidak</label>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf4"
                                    value="0.4" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf4">Mungkin Tidak</label>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf5"
                                    value="0.5" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf5">Kemungkinan Kecil</label>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf6"
                                    value="0.6" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf6">Mungkin</label>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf7"
                                    value="0.7" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf7">Kemungkinan Besar</label>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf8"
                                    value="0.8" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf8">Hampir Pasti</label>

                                <input type="radio" class="btn-check" name="g21" id="g21_cf9"
                                    value="1" autocomplete="off" disabled>
                                <label class="btn btn-outline-primary" for="g21_cf9">Pasti</label>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="https://www.instagram.com/sonyaputriily"
                    target="_blank" class="pe-1 text-primary text-decoration-underline">Sonya</a></p>
        </div>
        <!-- end row -->
    </div>
    <!-- container-fluid -->
@endsection


@push('scripts')
    <script>
        function enableAdditional(additionalId, oppositeRadioId) {
            document.getElementById(additionalId).style.display = "block";

            // Enable semua tombol radio di dalam additionalId
            var additionalRadios = document.querySelectorAll("#" + additionalId + " input[type=radio]");
            additionalRadios.forEach(function(radio) {
                radio.disabled = false;
            });

            // Uncheck tombol radio di dalam oppositeRadioId
            document.getElementById(oppositeRadioId).checked = false;
        }

        function disableAdditional(additionalId, oppositeRadioId) {
            document.getElementById(additionalId).style.display = "none";

            // Disable semua tombol radio di dalam additionalId
            var additionalRadios = document.querySelectorAll("#" + additionalId + " input[type=radio]");
            additionalRadios.forEach(function(radio) {
                radio.disabled = true;
            });

            // Uncheck tombol radio di dalam oppositeRadioId
            document.getElementById(oppositeRadioId).checked = false;
        }
    </script>
@endpush
