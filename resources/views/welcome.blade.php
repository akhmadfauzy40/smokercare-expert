@extends('layouts.master')


@section('content')
    <div class="container-fluid">
        <!--  Row 1 -->
        <div class="row">
            <div class="col-lg-8 d-flex align-items-strech">
                <div class="card w-100">
                    <div class="card-body">
                        <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                            <div class="mb-3 mb-sm-0">
                                <h5 class="card-title fw-semibold">Grafik Dugaan Penyakit Terhadap Pengguna</h5>
                            </div>
                        </div>
                        <div id="chart"></div>
                        <div class="legend-container mt-4">
                            <div>
                                <span
                                    style="display:inline-block; width:12px; height:12px; background-color: #5D87FF; border-radius: 50%; margin-right: 5px;"></span>
                                Pria
                            </div>
                            <div>
                                <span
                                    style="display:inline-block; width:12px; height:12px; background-color: #FF5D87; border-radius: 50%; margin-right: 5px;"></span>
                                Wanita
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Yearly Breakup -->
                        <div class="card overflow-hidden">
                            <div class="card-body p-4">
                                <h5 class="card-title mb-9 fw-semibold">BKPM (Balai Kesehatan Paru Masyarakat) Purwokerto
                                </h5>
                                <div class="row align-items-center">
                                    <div class="col-12">
                                        <div class="d-flex align-items-center">
                                            <p>alamat : Jl. A. Yani No.33, Karangjengkol, Sokanegara, Kec. Purwokerto Tim.,
                                                Kabupaten Banyumas, Jawa Tengah 53127, Indonesia</p>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <p>no telepon : (0281) 635658</p>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="d-flex justify-content-center">
                                            <div id="breakup"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="py-6 px-6 text-center">
            <p class="mb-0 fs-4">Developed by <a href="https://www.instagram.com/sonyaputriily" target="_blank"
                    class="pe-1 text-primary text-decoration-underline">Sonya</a></p>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // =====================================
        // Profit
        // =====================================
        var chart = {
            series: [{
                    name: "Pria:",
                    data: [{{ $data->pria_pneumonia }}, {{ $data->pria_bronkitis }},
                        {{ $data->pria_tuberkolosis_paru }},
                        {{ $data->pria_emfisema }}, {{ $data->pria_abses_paru }}
                    ]
                },
                {
                    name: "Wanita:",
                    data: [{{ $data->wanita_pneumonia }}, {{ $data->wanita_bronkitis }},
                        {{ $data->wanita_tuberkolosis_paru }}, {{ $data->wanita_emfisema }},
                        {{ $data->wanita_abses_paru }}
                    ]
                },
            ],

            chart: {
                type: "bar",
                height: 345,
                offsetX: -15,
                toolbar: {
                    show: true
                },
                foreColor: "#adb0bb",
                fontFamily: 'inherit',
                sparkline: {
                    enabled: false
                },
            },


            colors: ["#5D87FF", "#FF5D87"],


            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: "35%",
                    borderRadius: [6],
                    borderRadiusApplication: 'end',
                    borderRadiusWhenStacked: 'all'
                },
            },
            markers: {
                size: 0
            },

            dataLabels: {
                enabled: false,
            },


            legend: {
                show: false,
            },


            grid: {
                borderColor: "rgba(0,0,0,0.1)",
                strokeDashArray: 3,
                xaxis: {
                    lines: {
                        show: false,
                    },
                },
            },

            xaxis: {
                type: "category",
                categories: ["Pneumonia", "Bronkitis", "Tuberkolosis Paru", "Emfisema", "Abses Paru"],
                labels: {
                    style: {
                        cssClass: "grey--text lighten-2--text fill-color"
                    },
                },
            },


            yaxis: {
                show: true,
                min: 0,
                max: {{ $max }},
                tickAmount: 4,
                labels: {
                    style: {
                        cssClass: "grey--text lighten-2--text fill-color",
                    },
                },
            },
            stroke: {
                show: true,
                width: 3,
                lineCap: "butt",
                colors: ["transparent"],
            },


            tooltip: {
                theme: "light"
            },

            responsive: [{
                breakpoint: 600,
                options: {
                    plotOptions: {
                        bar: {
                            borderRadius: 3,
                        }
                    },
                }
            }]


        };

        var chart = new ApexCharts(document.querySelector("#chart"), chart);
        chart.render();
    </script>
@endpush
