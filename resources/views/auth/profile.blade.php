@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">My Profile</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="card-title">Profile Information</h4>

                                <!-- Display User Profile Information -->
                                <div class="mb-3">
                                    <strong>Name:</strong> {{ $data_profile->name }}
                                </div>
                                <div class="mb-3">
                                    <strong>Gender:</strong> {{ $data_profile->gender }}
                                </div>
                                <div class="mb-3">
                                    <strong>Username:</strong> {{ $data_profile->username }}
                                </div>

                                <!-- Button to Update Profile -->
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal">
                                    Update Profile
                                </button>
                            </div>

                            <div class="col-md-6">
                                <h4 class="card-title">Change Password</h4>

                                <!-- Button to Change Password -->
                                <button type="button" class="btn btn-warning" data-bs-toggle="modal"
                                    data-bs-target="#changePassword">
                                    Change Password
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- container-fluid -->

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/profile/edit">
                        @csrf <!-- Tambahkan csrf token untuk keamanan -->
                        @method('put')

                        <div class="mb-3">
                            <label for="exampleInputtext1" class="form-label">Nama</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                id="exampleInputtext1" name="name" value="{{ $data_profile->name }}"
                                aria-describedby="textHelp">
                            @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="" class="form-label">Gender</label>
                            <select id="gender" class="form-select @error('gender') is-invalid @enderror" name="gender">
                                <option value="pria" @if ($data_profile->gender == 'pria') selected @endif>Pria</option>
                                <option value="wanita" @if ($data_profile->gender == 'wanita') selected @endif>Wanita</option>
                            </select>
                            @error('gender')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Username</label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror"
                                id="exampleInputEmail1" name="username" value="{{ $data_profile->username }}"
                                aria-describedby="emailHelp">
                            @error('username')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changePassword" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/profile/editpassword" method="post">
                        @csrf
                        @method('put')
                        <div class="mb-4">
                            <label for="exampleInputPassword1" class="form-label">Password Sekarang</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="passwordold">
                        </div>
                        <div class="mb-4">
                            <label for="exampleInputPassword1" class="form-label">Password Baru</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="passwordnew">
                        </div>
                        <div class="mb-4">
                            <label for="exampleInputPassword1" class="form-label">Konfirmasi Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1"
                                name="passwordconfirm">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/swal.min.js') }}"></script>
    @if (session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: "{{ session('success') }}",
                timer: 3000, // Pesan akan hilang setelah 3 detik
                showConfirmButton: false // Tidak menampilkan tombol OK
            });
        </script>
    @endif
    @if (session('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: "{{ session('error') }}",
                timer: 3000, // Pesan akan hilang setelah 3 detik
                showConfirmButton: false // Tidak menampilkan tombol OK
            });
        </script>
    @endif
@endpush
