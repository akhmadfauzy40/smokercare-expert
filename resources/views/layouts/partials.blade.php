<nav class="sidebar-nav scroll-sidebar" data-simplebar="">
    <ul id="sidebarnav">
      <li class="nav-small-cap">
        <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
        <span class="hide-menu">Home</span>
      </li>
      <li class="sidebar-item">
        <a class="sidebar-link" href="/dashboard" aria-expanded="false">
          <span>
            <i class="ti ti-layout-dashboard"></i>
          </span>
          <span class="hide-menu">Dashboard</span>
        </a>
      </li>
      <li class="nav-small-cap">
        <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
        <span class="hide-menu">Menu Utama</span>
      </li>
      <li class="sidebar-item">
        <a class="sidebar-link" href="/tes-kesehatan" aria-expanded="false">
          <span>
            <i class="ti ti-article"></i>
          </span>
          <span class="hide-menu">Tes Kesehatan</span>
        </a>
      </li>
      <li class="sidebar-item">
        <a class="sidebar-link" href="/hasil-tes" aria-expanded="false">
          <span>
            <i class="ti ti-file-description"></i>
          </span>
          <span class="hide-menu">Hasil Tes Kesehatan</span>
        </a>
      </li>
    </ul>
  </nav>
