<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('test_gejala', function (Blueprint $table) {
            $table->id();
            $table->decimal('g01', 2,1)->default(0);
            $table->decimal('g02', 2,1)->default(0);
            $table->decimal('g03', 2,1)->default(0);
            $table->decimal('g04', 2,1)->default(0);
            $table->decimal('g05', 2,1)->default(0);
            $table->decimal('g06', 2,1)->default(0);
            $table->decimal('g07', 2,1)->default(0);
            $table->decimal('g08', 2,1)->default(0);
            $table->decimal('g09', 2,1)->default(0);
            $table->decimal('g10', 2,1)->default(0);
            $table->decimal('g11', 2,1)->default(0);
            $table->decimal('g12', 2,1)->default(0);
            $table->decimal('g13', 2,1)->default(0);
            $table->decimal('g14', 2,1)->default(0);
            $table->decimal('g15', 2,1)->default(0);
            $table->decimal('g16', 2,1)->default(0);
            $table->decimal('g17', 2,1)->default(0);
            $table->decimal('g18', 2,1)->default(0);
            $table->decimal('g19', 2,1)->default(0);
            $table->decimal('g20', 2,1)->default(0);
            $table->decimal('g21', 2,1)->default(0);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('test_gejala');
    }
};
