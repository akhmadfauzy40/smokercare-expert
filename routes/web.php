<?php

use App\Http\Controllers\auth_controller;
use App\Http\Controllers\Health_Test_Controller;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\welcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login');

Route::post('/login', [auth_controller::class, 'login']);
Route::get('/register', function () {
    return view('auth.register');
});
Route::post('/register/store', [auth_controller::class, 'store']);

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [welcomeController::class, 'index']);
    Route::get('/logout', [auth_controller::class, 'logout']);

    Route::get('/profile', [ProfileController::class, 'index']);

    Route::put('/profile/edit', [ProfileController::class, 'update']);
    Route::put('/profile/editpassword', [ProfileController::class, 'updatePassword']);

    Route::get('/tes-kesehatan', function () {
        return view('tes-kesehatan.index');
    });

    Route::get('/start-test', function () {
        return view('tes-kesehatan.form-test');
    });

    Route::get('/details', function () {
        return view('hasil-tes.detail');
    });

    Route::post('/submit-health-test', [Health_Test_Controller::class, 'save']);

    Route::get('/hasil-tes', [Health_Test_Controller::class, 'index']);
    Route::get('/detail/{id}', [Health_Test_Controller::class, 'detail']);
    Route::get('/detail-test/{id}', [Health_Test_Controller::class, 'detail_jawaban']);
});
